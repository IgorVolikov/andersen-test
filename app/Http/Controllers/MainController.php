<?php

namespace App\Http\Controllers;

use App\Models\Messages;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function sendmessage() {
        //return view('sendmessage');
        $allMessages = new Messages();
        return view('sendmessage', ['allMessages' => $allMessages->all()]);
    }

    public function sendmessage_send(Request $request) {
        //dd($request);

        $valid = $request->validate([
            'username' => 'required|min:4|max:100',
            'email' => 'required|min:4|max:100',
            'message' => 'required|min:5|max:500'
        ]);

        $message = new Messages();
        $message->username = $request->input('username');
        $message->email = $request->input('email');
        $message->message = $request->input('message');

        $message->save();

        return redirect()->route('sendmessage');
    }
}
