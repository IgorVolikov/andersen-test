<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/sendmessage', 'App\Http\Controllers\MainController@sendmessage')->name('sendmessage');
Route::post('/sendmessage/send', 'App\Http\Controllers\MainController@sendmessage_send');
